public class Kata {
  public static String solution(String str) {
    String palabraInvertida = "";
    for (int i = str.length() - 1; i >= 0; i--) {
      palabraInvertida += str.charAt(i);
    }
    return palabraInvertida;
  }
}
