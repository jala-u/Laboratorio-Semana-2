public class Game {
    public String winner(String[] deckSteve, String[] deckJosh) {
        // Initialize the scores
        int scoreSteve = 0;
        int scoreJosh = 0;

        // Compare the cards one by one
        for (int i = 0; i < deckSteve.length; i++) {
            int valueSteve = getCardValue(deckSteve[i]);
            int valueJosh = getCardValue(deckJosh[i]);

            if (valueSteve > valueJosh) {
                scoreSteve++;
            } else if (valueSteve < valueJosh) {
                scoreJosh++;
            }
        }

        // Determine the winner
        if (scoreSteve > scoreJosh) {
            return "Steve wins " + scoreSteve + " to " + scoreJosh;
        } else if (scoreSteve < scoreJosh) {
            return "Josh wins " + scoreJosh + " to " + scoreSteve;
        } else {
            return "Tie";
        }
    }

    private int getCardValue(String card) {
        switch (card) {
            case "2": return 2;
            case "3": return 3;
            case "4": return 4;
            case "5": return 5;
            case "6": return 6;
            case "7": return 7;
            case "8": return 8;
            case "9": return 9;
            case "T": return 10;
            case "J": return 11;
            case "Q": return 12;
            case "K": return 13;
            case "A": return 14;
            default: return 0;
        }
    }
}
